/* Calculator with extra functions */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <assert.h>
#include <limits.h>
#include <math.h>
#include <float.h>

enum { Add, Subtract, Multiply, Divide, Exponent, Squareroot, Error, Quad, Decimal };

bool digit(char c) {
    if (isdigit(c) == 0) return false;
    else return true;
}

long long convertl(const char n[]){
    return atol(n);
}

double convertf(const char n[]){
  return atof(n);
}

bool valid(const char x[]){
    bool valid = 1;
    for (size_t i = 0; x[i] != '\0'; i++) {
      if (digit(x[i]) == 0) valid = 0;
      else valid = 1;
    }
    long long y = convertl(x);
    if ((y > LLONG_MAX) || (y < LLONG_MIN)) valid = 0;
    return valid;
}

bool doublevalid(const char x[]){
  bool valid = 1;
  for (size_t i = 0; x[i] != '\0'; i++) {
    if (digit(x[i]) == 0) valid = 0;
    else valid = 1;
  }
  int i, count;
  for (i = 0, count = 0; x[i]; i++) {
    count += (x[i] == '.');
  }
  if (count > 1) valid = 0;
  double y = convertf(x);
  if (y < 0) y = (y * -1) - 1;
  if ((y > DBL_MAX)) valid = 0;
  return valid;
}

long long add(long long x, long long y){
  return x + y;
}

double doubleadd(double x, double y){
  return x + y;
}

long long subtract(long long x, long long y){
  return x - y;
}

double doublesubtract(double x, double y){
  return x - y;
}

long long multiply(long long x, long long y){
  return x * y;
}

double doublemultiply(double x, double y){
  return x * y;
}

long long divide(long long x, long long y){
  if (y == 0) return Error;
  else return x / y;
}

double doubledivide(double x, double y){
  if (y == 0) return Error;
  else return x / y;
}

long long exponent(long long x, long long y){
  long long ans = 1;
  for (size_t i = 0; i < y; i++) {
    ans = ans * x;
  }
  return ans;
}

double doubleexponent(double x, double y){
  double ans = 1;
  for (size_t i = 0; i < y; i++) {
    ans = ans * x;
  }
  return ans;
}

double squareroot(char ia[]){
  double x = convertf(ia);
  if ((valid(ia) == 0) || (x < 0)) return -1;
  else return sqrt(x);
}

double posquad(char ia[], char ib[], char ic[]){
  double x = convertf(ia);
  double y = convertf(ib);
  double z = convertf(ic);
  if ((valid(ia) == 0) || (valid(ib) == 0) || (valid(ic) == 0)) return Error;
  double a = -y;
  double b = y*y;
  double c = 4*(x * z);
  double d = b - c;
  double e = sqrt(d);
  double f = a + e;
  double g = 2 * x;
  double ans = f / g;
  return ans;
}

double negquad(char ia[], char ib[], char ic[]){
  double x = convertf(ia);
  double y = convertf(ib);
  double z = convertf(ic);
  if ((valid(ia) == 0) || (valid(ib) == 0) || (valid(ic) == 0)) return Error;
  double a = -y;
  double b = y*y;
  double c = 4*(x * z);
  double d = b - c;
  double e = sqrt(d);
  double f = a - e;
  double g = 2 * x;
  double ans = f / g;
  return ans;
}

bool anscheck(int op, long long answer, const char x[], const char y[]){
  bool check = 1;
  long long input1 = convertl(x);
  long long input2 = convertl(y);
  if (op == Add) {
    if ((input1 > 0) && (input2 > 0) && (answer <= 0)) check = 0;
    else if ((input1 < 0) && (input2 < 0) && (answer >= 0)) check = 0;
    else if ((input1 == -input2) && (answer != 0)) check = 0;
    else if ((answer == 6) && (add(input1, input2) != 6)) check = 0;
    else check = 1;
  }
  else if (op == Subtract) {
    if ((input1 > 0) && (input2 > 0) && (input1 > input2) && (answer <= 0)) check = 0;
    else if ((input1 > 0) && (input2 > 0) && (input2 > input1) && (answer >= 0)) check = 0;
    else if ((input1 == input2) && (answer != 0)) check = 0;
    else if ((input1 < 0) && (input2 < 0) && (input1 < input2) && (answer >= 0)) check = 0;
    else if ((input1 < 0) && (input2 < 0) && (input2 < input1) && (answer <= 0)) check = 0;
    else if ((answer == 6) && (subtract(input1, input2) != 6)) check = 0;
    else check = 1;
  }
  else if (op == Multiply) {
    if ((input1 > 0) && (input2 > 0) && (answer <= 0)) check = 0;
    else if ((input1 > 0) && (input2 < 0) && (answer >= 0)) check = 0;
    else if ((input1 < 0) && (input2 > 0) && (answer >= 0)) check = 0;
    else if ((input1 < 0) && (input2 < 0) && (answer <= 0)) check = 0;
    else if (((input1 == 0) || (input2 == 0)) && (answer != 0)) check = 0;
    else if ((answer == 6) && (multiply(input1, input2) != 6)) check = 0;
    else check = 1;
  }
  else if (op == Divide) {
    if ((input1 > 0) && (input2 > 0) && (answer <= 0)) check = 0;
    else if ((input1 > 0) && (input2 < 0) && (answer >= 0)) check = 0;
    else if ((input1 < 0) && (input2 > 0) && (answer >= 0)) check = 0;
    else if ((input1 < 0) && (input2 < 0) && (answer <= 0)) check = 0;
    else if ((input1 == 0) && (answer != 0)) check = 0;
    else if ((answer == 6) && (divide(input1, input2) != 6)) check = 0;
    else check = 1;
  }
  else if (op == Exponent) {
    if (input2 > 0){
      //if (input2 < 0) check = 0;
      if ((answer == 6) && (exponent(input1, input2) != 6)) check = 0;
      else check = 1;
    }
    else check = 0;
  }
  //printf("%d\n", check);
  return check;
}

bool danscheck(int op, double answer, const char x[], const char y[]){
  bool check = 1;
  double input1 = convertf(x);
  double input2 = convertf(y);
  if (op == Add) {
    if ((input1 > 0) && (input2 > 0) && (answer <= 0)) check = 0;
    else if ((input1 < 0) && (input2 < 0) && (answer >= 0)) check = 0;
    else if ((input1 == -input2) && (answer != 0)) check = 0;
    else if ((answer == 6) && (doubleadd(input1, input2) != 6)) check = 0;
    else check = 1;
  }
  else if (op == Subtract) {
    if ((input1 > 0) && (input2 > 0) && (input1 > input2) && (answer <= 0)) check = 0;
    else if ((input1 > 0) && (input2 > 0) && (input2 > input1) && (answer >= 0)) check = 0;
    else if ((input1 == input2) && (answer != 0)) check = 0;
    else if ((input1 < 0) && (input2 < 0) && (input1 < input2) && (answer >= 0)) check = 0;
    else if ((input1 < 0) && (input2 < 0) && (input2 < input1) && (answer <= 0)) check = 0;
    else if ((answer == 6) && (doublesubtract(input1, input2) != 6)) check = 0;
    else check = 1;
  }
  else if (op == Multiply) {
    if ((input1 > 0) && (input2 > 0) && (answer <= 0)) check = 0;
    else if ((input1 > 0) && (input2 < 0) && (answer >= 0)) check = 0;
    else if ((input1 < 0) && (input2 > 0) && (answer >= 0)) check = 0;
    else if ((input1 < 0) && (input2 < 0) && (answer <= 0)) check = 0;
    else if (((input1 == 0) || (input2 == 0)) && (answer != 0)) check = 0;
    else if ((answer == 6) && (doublemultiply(input1, input2) != 6)) check = 0;
    else check = 1;
  }
  else if (op == Divide) {
    if ((input1 > 0) && (input2 > 0) && (answer <= 0)) check = 0;
    else if ((input1 > 0) && (input2 < 0) && (answer >= 0)) check = 0;
    else if ((input1 < 0) && (input2 > 0) && (answer >= 0)) check = 0;
    else if ((input1 < 0) && (input2 < 0) && (answer <= 0)) check = 0;
    else if ((input1 == 0) && (answer != 0)) check = 0;
    else if ((answer == 6) && (doubledivide(input1, input2) != 6)) check = 0;
    else check = 1;
  }
  else if (op == Exponent) {
    if (input2 > 0){
      if ((answer == 6) && (doubleexponent(input1, input2) != 6)) check = 0;
      else check = 1;
    }
    else check = 0;
  }
  return check;
}

bool quadcheck(double answer1, double answer2, char x[], char y[], char z[]){
  bool check = 1;
  if ((answer1 == 6) && (posquad(x, y, z) != 6)) check = 0;
  else if ((answer2 == 6) && (negquad(x, y, z) != 6)) check = 0;
  else check = 1;
  return check;
}

long long calculate(int ia, char ib[], char ic[]){
  long long x = convertl(ib);
  long long y = convertl(ic);
  //printf("%s %s\n", ib, ic);
  if ((valid(ib) == 0) || (valid(ic) == 0)) return Error;
  else if (ia == Add) return add(x, y);
  else if (ia == Subtract) return subtract(x, y);
  else if (ia == Multiply) return multiply(x, y);
  else if (ia == Divide) return divide(x, y);
  else if ((ia == Exponent) && (y >= 0)) return exponent(x, y);
  else return 0;
}

double doublecalculate(int ia, char ib[], char ic[]){
  double x = convertf(ib);
  double y = convertf(ic);
  if ((doublevalid(ib) == 0) || (doublevalid(ic) == 0)) return Error;
  else if (ia == Add) return doubleadd(x, y);
  else if (ia == Subtract) return doublesubtract(x, y);
  else if (ia == Multiply) return doublemultiply(x, y);
  else if (ia == Divide) return doubledivide(x, y);
  else if (ia == Exponent) return doubleexponent(x, y);
  else return 0;
}

int operations(char operation[]){
  if (strcmp("add", operation) == 0) return Add;
  else if (strcmp("subtract", operation) == 0) return Subtract;
  else if (strcmp("multiply", operation) == 0) return Multiply;
  else if (strcmp("divide", operation) == 0) return Divide;
  else if (strcmp("power", operation) == 0) return Exponent;
  else if (strcmp("sqrt", operation) == 0) return Squareroot;
  else if (strcmp("quad", operation) == 0) return Quad;
  else return Error;
}

int hasdecimal(char decimal1[], char decimal2[]){
  char *x;
  char *y;
  x = strstr(decimal1, ".");
  y = strstr(decimal2, ".");
  if ((x != NULL) || (y != NULL)) return Decimal;
  else return -1;
}

void checkConstants() {
    assert(Add == 0 && Subtract == 1 && Multiply == 2 && Divide == 3 && Exponent == 4);
    assert(Squareroot == 5 && Error == 6 && Quad == 7 && Decimal == 8);
}

void testAdd() {
    char max[317] = "179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000";
    char min[318] = "-179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000";
    double dmax = 179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000;
    double dmin = -179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000;
    assert(calculate(Add, "0", "0") == 0);
    assert(calculate(Add, "1", "1") == 2);
    assert(calculate(Add, "1024", "1024") == 2048);
    assert(calculate(Add, "-2048", "1024") == -1024);
    assert(calculate(Add, "9223372036854775807", "0") == 9223372036854775807);
    assert(calculate(Add, "0", "9223372036854775807") == 9223372036854775807);
    assert(calculate(Add, "-9223372036854775807", "0") == -9223372036854775807);
    assert(calculate(Add, "0", "-9223372036854775807") == -9223372036854775807);
    assert(calculate(Add, "x", "1") == Error);
    assert(calculate(Add, "1", "x") == Error);
    assert(calculate(Add, "x", "x") == Error);
    assert(doublecalculate(Add, "0", "0") == 0);
    assert(doublecalculate(Add, "1", "1") == 2);
    assert(doublecalculate(Add, "1024", "1024") == 2048);
    assert(doublecalculate(Add, "-2048", "1024") == -1024);
    assert(doublecalculate(Add, max, "0") == dmax);
    assert(doublecalculate(Add, "0", max) == dmax);
    assert(doublecalculate(Add, min, "0") == dmin);
    assert(doublecalculate(Add, "0", min) == dmin);
    assert(doublecalculate(Add, "x", "1") == Error);
    assert(doublecalculate(Add, "1", "x") == Error);
    assert(doublecalculate(Add, "x", "x") == Error);
}

void testSubtract() {
    char max[317] = "179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000";
    char min[318] = "-179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000";
    double dmax = 179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000;
    double dmin = -179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000;
    assert(calculate(Subtract, "0", "0") == 0);
    assert(calculate(Subtract, "1", "1") == 0);
    assert(calculate(Subtract, "1024", "2048") == -1024);
    assert(calculate(Subtract, "-1024", "2048") == -3072);
    assert(calculate(Subtract, "9223372036854775807", "0") == 9223372036854775807);
    assert(calculate(Subtract, "0", "9223372036854775807") == -9223372036854775807);
    assert(calculate(Subtract, "-9223372036854775807", "0") == -9223372036854775807);
    assert(calculate(Subtract, "0", "-9223372036854775807") == 9223372036854775807);
    assert(calculate(Subtract, "x", "1") == Error);
    assert(calculate(Subtract, "1", "x") == Error);
    assert(calculate(Subtract, "x", "x") == Error);
    assert(doublecalculate(Subtract, "0", "0") == 0);
    assert(doublecalculate(Subtract, "1", "1") == 0);
    assert(doublecalculate(Subtract, "1024", "2048") == -1024);
    assert(doublecalculate(Subtract, "-1024", "2048") == -3072);
    assert(doublecalculate(Subtract, max, "0") == dmax);
    assert(doublecalculate(Subtract, "0", max) == dmin);
    assert(doublecalculate(Subtract, min, "0") == dmin);
    assert(doublecalculate(Subtract, "0", min) == dmax);
    assert(doublecalculate(Subtract, "x", "1") == Error);
    assert(doublecalculate(Subtract, "1", "x") == Error);
    assert(doublecalculate(Subtract, "x", "x") == Error);
}

void testMultiply() {
    char max[317] = "179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000";
    char min[318] = "-179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000";
    double dmax = 179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000;
    double dmin = -179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000;
    assert(calculate(Multiply, "0", "0") == 0);
    assert(calculate(Multiply, "1", "1") == 1);
    assert(calculate(Multiply, "256", "128") == 32768);
    assert(calculate(Multiply, "-1024", "512") == -524288);
    assert(calculate(Multiply, "9223372036854775807", "1") == 9223372036854775807);
    assert(calculate(Multiply, "1", "9223372036854775807") == 9223372036854775807);
    assert(calculate(Multiply, "-9223372036854775807", "1") == -9223372036854775807);
    assert(calculate(Multiply, "1", "-9223372036854775807") == -9223372036854775807);
    assert(calculate(Multiply, "x", "1") == Error);
    assert(calculate(Multiply, "1", "x") == Error);
    assert(calculate(Multiply, "x", "x") == Error);
    assert(doublecalculate(Multiply, "0", "0") == 0);
    assert(doublecalculate(Multiply, "1", "1") == 1);
    assert(doublecalculate(Multiply, "256", "128") == 32768);
    assert(doublecalculate(Multiply, "-1024", "512") == -524288);
    assert(doublecalculate(Multiply, max, "1") == dmax);
    assert(doublecalculate(Multiply, "1", max) == dmax);
    assert(doublecalculate(Multiply, min, "1") == dmin);
    assert(doublecalculate(Multiply, "1", min) == dmin);
    assert(doublecalculate(Multiply, "x", "1") == Error);
    assert(doublecalculate(Multiply, "1", "x") == Error);
    assert(doublecalculate(Multiply, "x", "x") == Error);
}

void testDivide() {
    char max[317] = "179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000";
    char min[318] = "-179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000";
    double dmax = 179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000;
    double dmin = -179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000;
    assert(calculate(Divide, "0", "1") == 0);
    assert(calculate(Divide, "1", "1") == 1);
    assert(calculate(Divide, "256", "128") == 2);
    assert(calculate(Divide, "-1024", "512") == -2);
    assert(calculate(Divide, "9223372036854775807", "1") == 9223372036854775807);
    assert(calculate(Divide, "-9223372036854775807", "1") == -9223372036854775807);
    assert(calculate(Divide, "1", "0") == Error);
    assert(calculate(Divide, "0", "0") == Error);
    assert(calculate(Divide, "x", "1") == Error);
    assert(calculate(Divide, "1", "x") == Error);
    assert(calculate(Divide, "x", "x") == Error);
    assert(doublecalculate(Divide, "0", "1") == 0);
    assert(doublecalculate(Divide, "1", "1") == 1);
    assert(doublecalculate(Divide, "256", "128") == 2);
    assert(doublecalculate(Divide, "-1024", "512") == -2);
    assert(doublecalculate(Divide, max, "1") == dmax);
    assert(doublecalculate(Divide, min, "1") == dmin);
    assert(doublecalculate(Divide, "1", "0") == Error);
    assert(doublecalculate(Divide, "0", "0") == Error);
    assert(doublecalculate(Divide, "x", "1") == Error);
    assert(doublecalculate(Divide, "1", "x") == Error);
    assert(doublecalculate(Divide, "x", "x") == Error);
}

void testExponent(){
  char max[317] = "179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000";
  char min[318] = "-179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000";
  double dmax = 179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000;
  double dmin = -179769313486231570814527423731704356798070600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000000;
  assert(calculate(Exponent, "0", "1") == 0);
  assert(calculate(Exponent, "1", "1") == 1);
  assert(calculate(Exponent, "1", "0") == 1);
  assert(calculate(Exponent, "2", "10") == 1024);
  assert(calculate(Exponent, "-2", "15") == -32768);
  assert(calculate(Exponent, "9223372036854775807", "1") == 9223372036854775807);
  assert(calculate(Exponent, "-9223372036854775807", "1") == -9223372036854775807);
  assert(calculate(Exponent, "x", "1") == Error);
  assert(calculate(Exponent, "1", "x") == Error);
  assert(calculate(Exponent, "x", "x") == Error);
  assert(doublecalculate(Exponent, "0", "1") == 0);
  assert(doublecalculate(Exponent, "1", "1") == 1);
  assert(doublecalculate(Exponent, "1", "0") == 1);
  assert(doublecalculate(Exponent, "2", "10") == 1024);
  assert(doublecalculate(Exponent, "-2", "15") == -32768);
  assert(doublecalculate(Exponent, max, "1") == dmax);
  assert(doublecalculate(Exponent, min, "1") == dmin);
  assert(doublecalculate(Exponent, "x", "1") == Error);
  assert(doublecalculate(Exponent, "1", "x") == Error);
  assert(doublecalculate(Exponent, "x", "x") == Error);
}

void testRoot(){
  double a = 1125899906842624;
  assert(squareroot("4") == 2);
  assert(squareroot("1267650600228229401496703205376") == a);
  assert(squareroot("-4") == -1);
  assert(squareroot("x") == -1);
}

void testQuad(){
  assert(posquad("1", "1", "-12") == 3);
  assert(negquad("1", "1", "-12") == -4);
  assert(posquad("2", "-28", "98") == 7);
  assert(negquad("2", "-28", "98") == 7);
  assert(posquad("5", "-20", "15") == 3);
  assert(negquad("5", "-20", "15") == 1);
}

// Run tests on the calculate function.
void test() {
    checkConstants();
    testAdd();
    testSubtract();
    testMultiply();
    testDivide();
    testExponent();
    testRoot();
    testQuad();
    //testEquilateral();
    //testIsosceles();
    //testRight();
    //testScalene();
    //testFlat();
    //testImpossible();
    //testIllegal();
    //testLimits();
    //testOverflow();
    printf("All tests passed\n");
}

// Run the program or, if there are no arguments, test it.
int main(int n, char **argv) {
    if (n == 1) {
      test();
    }
    else if (n == 3) {
      double result = squareroot(argv[2]);
      if (result == -1) printf("Illegal\n");
      else printf("%f\n", result);
    }
    else if (n == 4){
      int x = operations(argv[1]);
      int y = hasdecimal(argv[2], argv[3]);
      if (x == Error) printf("Operation not Found\n");
      else {
        if (y == Decimal){
          double dresult = calculate(x, argv[2], argv[3]);
          bool check = danscheck(x, dresult, argv[2], argv[3]);
          if ((dresult == 6) && (check == 1)) {
            double a = 6;
            printf("%f\n", a);
          }
          else if (check == 1) printf("%f\n", dresult);
          else printf("Error\n");
        }
        else {
          long long result = calculate(x, argv[2], argv[3]);
          bool check = anscheck(x, result, argv[2], argv[3]);
          if ((result == 6) && (check == 1)) {
            long long a = 6;
            printf("%lld\n", a);
          }
          else if (check == 1) printf("%lld\n", result);
          else printf("Error\n");
        }
      }
    }
    else if (n == 5){
      int x = operations(argv[1]);
      if (x == Quad){
        double result1 = (posquad(argv[2], argv[3], argv[4]));
        double result2 = (negquad(argv[2], argv[3], argv[4]));
        bool check = quadcheck(result1, result2, argv[2], argv[3], argv[4]);
        if ((result1 == 6) && (check == 1)){
          double a = 6;
          printf("x = %f\nx = %f\n", a, result2);
        }
        if ((result2 == 6) && (check == 1)){
          double a = 6;
          printf("x = %f\nx = %f\n", result1, a);
        }
        else if ((result1 == Error) || (result2 == Error)) printf("Error\n");
        else if ((result1 != result1) || (result2 != result2)) printf("No real solution found\n");
        else if (result1 == result2) printf("x = %f\n", result1);
        else printf("x = %f\nx = %f\n", result1, result2);
      }
      else printf("Error\n");
    }
    else {
        fprintf(stderr, "For help refer to the readme.pdf document\n ");
        exit(1);
    }
    return 0;
}
